﻿# EditDataFileFromWorkflow

## Component Information
* Created by: Mythics
* Author: Jonathan Hult
* License: MIT
* Last Updated: build_1_20130917

## Overview
This WebCenter Content component adds a menu item to allow editing a Data File from the following pages: Content Items in Workflow and Workflow Review. 
	
* Dynamichtml includes:
	- custom_wf_review_links: Core - override to add Edit Data File to Workflow Review page
	- custom_add_to_action_popup_data: Core - override to add Edit Data File to Actions menu on Content in Workflow page
	
## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 11.1.1.8.0-2013-07-11 17:07:21Z-r106802 (Build: 7.3.5.185)
* 10.1.3.5.1 (111229) (Build:7.2.4.105) 

## Changelog
* build_1_20130917
	- Initial component release